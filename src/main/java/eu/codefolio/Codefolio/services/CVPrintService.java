package eu.codefolio.Codefolio.services;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import eu.codefolio.Codefolio.domains.Candidate;
import eu.codefolio.Codefolio.domains.Skill;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by verit on 07.12.2018.
 */
@Service
public class CVPrintService {

    Font fontello = FontFactory.getFont("fontello/font/fontello.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 11);
    Font fontello2 = FontFactory.getFont("fontello2/font/fontello.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 11);
    Font fontAwBrands = FontFactory.getFont("fontawesome-desktop/otfs/Brands-Regular-400.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12);
    Font fontAwRegular = FontFactory.getFont("fontawesome-desktop/otfs/Free-Regular-400.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12);
    Font fontAwSolid = FontFactory.getFont("fontawesome-desktop/otfs/Free-Solid-900.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 12);

    Font font = FontFactory.getFont(FontFactory.HELVETICA, 11, BaseColor.BLACK);
    Font fontH1 = FontFactory.getFont(FontFactory.HELVETICA, 14, BaseColor.BLACK);
    Font fontH2 = FontFactory.getFont(FontFactory.HELVETICA, 16, BaseColor.BLACK);


    public Document produceCV(Candidate candidate) throws IOException, DocumentException, URISyntaxException {


        Document pdf = new Document();
        PdfWriter.getInstance(pdf, new FileOutputStream(candidate.getName() + candidate.getSurname() + "_CV.pdf"));
        pdf.open();


        // BaseFont baseFont = BaseFont.createFont("fontello/font/fontello.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        // Font fontello = new Font(baseFont, 16, Font.NORMAL);
        //  BaseFont baseFont2 = BaseFont.createFont("fontawesome-desktop/otfs/Brands-Regular-400.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        //   Font fontAwesome = new Font(baseFont, 16, Font.NORMAL);


        Path employeePortraitPath = Paths.get(ClassLoader.getSystemResource("static/images/" + candidate.getPhoto()).toURI());
        Image emplyeePortrait = Image.getInstance(employeePortraitPath.toAbsolutePath().toString());
        Float originalWidth = emplyeePortrait.getWidth();
        Float originalHeight = emplyeePortrait.getHeight();
        Float ratio = originalHeight / originalWidth;
        Rectangle rectangle = new Rectangle(100, 100, 300, 100 + 200 * ratio);
        emplyeePortrait.scaleToFit(rectangle);

        Phrase careerGoal = new Phrase(candidate.getCareerGoal(), font);

        List<Chunk> skillChunks = new ArrayList();
        List<Skill> skills = candidate.getSkills();
        for (Skill skill : skills) {
            Chunk skillChunk = new Chunk(skill.getName() + " - " + skill.getProficiency(), font);
            skillChunks.add(skillChunk);
        }

        com.itextpdf.text.List skillsItext = new com.itextpdf.text.List();
      /*  for (Chunk skillChunk : skillChunks) {
            pdf.add(skillChunk);
        }*/


        PdfPTable table = new PdfPTable(2);

        PdfPCell cell = new PdfPCell(emplyeePortrait);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setPadding(30);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.addElement(new Paragraph(candidate.getName() + " " + candidate.getSurname(), fontH1));
        cell.addElement(new Paragraph(candidate.getContact().getPhone(), fontH2));
        cell.addElement(new Paragraph(candidate.getContact().getEmail(), fontH2));
        cell.addElement(new Paragraph(candidate.getContact().getLinkedIn(), fontH2));
        cell.addElement(new Paragraph(candidate.getContact().getGitLab(), fontH2));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPadding(30);
        table.addCell(cell);


        cell = new PdfPCell();
        cell.addElement(new Paragraph("Skills", fontH2));
        for (Skill skill : skills) {
            printSkill(skill.getName(), 2, cell);
        }
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setPadding(30);
        table.addCell(cell);

        cell = new PdfPCell(careerGoal);
        cell.addElement(new Paragraph("Career Goal", fontH2));
        cell.addElement(new Paragraph(candidate.getCareerGoal(), font));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setPadding(30);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setColspan(2);
        cell.addElement(new Paragraph("Interests", fontH2));
        cell.addElement(new Paragraph(candidate.getInterests(), font));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setPadding(30);
        table.addCell(cell);


        pdf.add(table);
        pdf.close();
        return pdf;
    }

    void printSkill(String name, Integer proficiencyLevel, PdfPCell cell) {
        //Paragraph skill = new Paragraph(name, fontello);
        PdfPTable table = new PdfPTable(2);

        Chunk skill = new Chunk(name, font);

        Chunk proficient = new Chunk("", fontello);
        for (int i = 0; i < proficiencyLevel; i++) {
            proficient.append("\uf10c");
        }

        for (int i = 0; i < proficiencyLevel; i++) {
            proficient.append("\uf111");
        }

        PdfPCell skillCell = new PdfPCell();
        skillCell.addElement(skill);
        skillCell.setBorder(Rectangle.NO_BORDER);
        table.addCell(skillCell);

        skillCell = new PdfPCell();
        skillCell.addElement(proficient);
        skillCell.setBorder(Rectangle.NO_BORDER);
        table.addCell(skillCell);


       /* skillCell = new PdfPCell();
        skillCell.addElement(nonProficient);
        skillCell.setBorder(Rectangle.NO_BORDER);
        table.addCell(skillCell);
*/
        cell.addElement(table);

    }
}

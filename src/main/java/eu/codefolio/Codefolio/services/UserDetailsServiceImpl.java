package eu.codefolio.Codefolio.services;

import eu.codefolio.Codefolio.domains.AutUser;
import eu.codefolio.Codefolio.domains.Role;
import eu.codefolio.Codefolio.persistence.AutUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by tomasz.szczucki on 05.04.2019.
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AutUserRepository userRepository;

    //username = email
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) {
        AutUser autUser = userRepository.findByEmail(username);
        if (autUser == null) throw new UsernameNotFoundException(username);

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (Role role : autUser.getRoles()){
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
        }

        return new User(autUser.getEmail(), autUser.getPassword(), grantedAuthorities);
    }
}

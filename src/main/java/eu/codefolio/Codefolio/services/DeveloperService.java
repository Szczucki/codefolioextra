package eu.codefolio.Codefolio.services;

import eu.codefolio.Codefolio.domains.Developer;
import eu.codefolio.Codefolio.persistence.DeveloperRepository;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by verit on 21.10.2018.
 */
@Service
public class DeveloperService {


    private Map<Integer, Developer> developerMap;

    @Autowired
    DeveloperRepository developerRepository;


    public Developer getDeveloper (String name) {
        name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
        return developerRepository.findByName(name);
    }

    public Developer getDeveloper (Long id) {
        return developerRepository.findById(id).get();
    }

    public Developer getDeveloperWAllRelations (Long id) {

        Developer dev = developerRepository.findById(id).get();
        Hibernate.initialize(dev.getEducationList());
        Hibernate.initialize(dev.getWorkExperienceList());
        Hibernate.initialize(dev.getAutUser());
        return dev;
    }

    public ArrayList<Developer> getDevelopers() {
        ArrayList<Developer> developers = new ArrayList();
        for (Developer dev : developerRepository.findAll()) {
            developers.add(dev);
        }
        return developers;
    }

    public Long persist (Developer developer) {
        developerRepository.save(developer);
        return developer.getId();
    }

    public Long delete (Developer developer) {
        developerRepository.delete(developer);
        return developer.getId();
    }

    public void delete (Long id) {
        developerRepository.deleteById(id);
    }

}

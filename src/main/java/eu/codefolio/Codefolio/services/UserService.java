package eu.codefolio.Codefolio.services;

import eu.codefolio.Codefolio.domains.AutUser;
import eu.codefolio.Codefolio.domains.Role;
import eu.codefolio.Codefolio.persistence.AutUserRepository;
import eu.codefolio.Codefolio.persistence.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.thymeleaf.expression.Sets;


import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


/**
 * Created by tomasz.szczucki on 27.03.2019.
 */
@Service
public class UserService {

    @Autowired
    AutUserRepository repository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    public Long persist(AutUser user) {
       // user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Iterator<Role> iterable = roleRepository.findAll().iterator();
        HashSet<Role> roles = new HashSet();
        iterable.forEachRemaining(roles :: add);
        user.setRoles(roles);
        repository.save(user);
        return user.getId();
    }

    public AutUser findByEmail(String email) {
        return repository.findByEmail(email);
    }
}

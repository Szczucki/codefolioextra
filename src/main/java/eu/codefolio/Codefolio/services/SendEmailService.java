package eu.codefolio.Codefolio.services;

import eu.codefolio.Codefolio.configurations.EmailProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;


import java.util.Properties;

/**
 * Created by verit on 29.11.2018.
 */
@Service
public class SendEmailService {

    @Autowired
    JavaMailSenderImpl mailSender;

    @Autowired
    EmailProperties emailProperties;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public void sendMail(String fromName, String fromEmail, String developerMail, String subject, String message) {

        SimpleMailMessage mail = new SimpleMailMessage();

        mailSender.setHost(emailProperties.getHost());
        mailSender.setPort(emailProperties.getPort());
        mailSender.setUsername(emailProperties.getUsername());
        mailSender.setPassword(emailProperties.getPassword());

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", emailProperties.getProtocol());
        props.put("mail.smtp.auth", emailProperties.getSmptAuth());
        props.put("mail.smtp.starttls.enable", emailProperties.getSmtpStarttlsEnable());
        props.put("mail.smtp.socketFactory.class", emailProperties.getSocketFactoryClass());

        mail.setTo(developerMail);
        mail.setSubject(subject);
        message = "From: " + fromName + "\n" + "Email: " + fromEmail + "\n" + message;
        mail.setText(message);

        logger.info("Sending...");

        mailSender.send(mail);

        logger.info("Done!");
    }

}
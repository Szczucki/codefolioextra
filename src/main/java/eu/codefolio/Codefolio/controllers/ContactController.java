package eu.codefolio.Codefolio.controllers;

import eu.codefolio.Codefolio.commands.ContactForm;
import eu.codefolio.Codefolio.services.DeveloperService;
import eu.codefolio.Codefolio.services.SendEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;

/**
 * Created by verit on 21.10.2018.
 */
@Controller
public class ContactController {

    @Autowired
    private DeveloperService developerService;

    @Autowired
    private SendEmailService sendEmailService;

    @RequestMapping("/contact/{name}")
    public String getContact(@PathVariable String name, Model model) {

        model.addAttribute("developer", developerService.getDeveloper(name));
        model.addAttribute("contactForm", new ContactForm());

        return "contact";
    }

    @RequestMapping("/contact/{name}/sendemail")
    public String sendEmail(@PathVariable String name, @Valid ContactForm contactForm, BindingResult bindingResult, Model model) throws UnsupportedEncodingException, MessagingException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("developer", developerService.getDeveloper(name));
            return "contact";
        }
        String developerEmail = developerService.getDeveloper(name).getContact().getEmail();
        sendEmailService.sendMail(contactForm.getName(),
                contactForm.getEmail(),
                developerEmail,
                contactForm.getSubject(),
                contactForm.getMessage());

        return "emailsent";
    }

}

package eu.codefolio.Codefolio.controllers;

import eu.codefolio.Codefolio.commands.CandidateBasicsForm;
import eu.codefolio.Codefolio.commands.ChooseCandidateForm;
import eu.codefolio.Codefolio.commands.EducationListForm;
import eu.codefolio.Codefolio.commands.EmploymentListForm;
import eu.codefolio.Codefolio.domains.Developer;
import eu.codefolio.Codefolio.domains.Candidate;
import eu.codefolio.Codefolio.domains.Education;
import eu.codefolio.Codefolio.domains.WorkExperience;
import eu.codefolio.Codefolio.services.DeveloperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by verit on 21.10.2018.
 */
@Controller
@SessionAttributes({"candidate", "educationListForm", "employmentListForm"})
public class EditCandidateController {
    @Autowired
    private DeveloperService developerService;

    @ModelAttribute("candidate")
    public Candidate getCandidate() {
        return new Developer();
    }

    @ModelAttribute("educationListForm")
    public EducationListForm getEducationListForm() {
        return new EducationListForm();
    }

    @ModelAttribute("employmentListForm")
    public EmploymentListForm getEmploymentListForm() {
        return new EmploymentListForm();
    }


    @RequestMapping("/chooseCandidate")
    public String chooseCandidate(Model model, SessionStatus status) {


        status.setComplete();
        List availableCandidates = developerService.getDevelopers();
        Candidate placeholderCandidate = new Developer();
        placeholderCandidate.setName("NEW");
        availableCandidates.add(placeholderCandidate);
        Collections.reverse(availableCandidates);
        model.addAttribute("availableCandidates", availableCandidates);
        model.addAttribute("chooseCandidateCommand", new ChooseCandidateForm());

        return "chooseCandidate";
    }


    @RequestMapping(value = "/editCandidate", method = POST)
    public String editCandidate(
            @RequestParam(value = "action", required = false) String action,
            @ModelAttribute("candidate") Candidate candidate,
            @ModelAttribute("educationListForm") EducationListForm educationListForm,
            ChooseCandidateForm form, Model model, SessionStatus status) {

        CandidateBasicsForm candidateBasicsForm = new CandidateBasicsForm();
        if ("edit".equals(action)) {
            if (form.getCandidateId() != null && form.getCandidateId() != 0) {
                candidate = developerService.getDeveloperWAllRelations(form.getCandidateId());
                candidateBasicsForm.setId(candidate.getId());
                candidateBasicsForm.setName(candidate.getName());
                candidateBasicsForm.setSurname(candidate.getSurname());
                candidateBasicsForm.setCareerGoal(candidate.getCareerGoal());
                candidateBasicsForm.setDateOfBirth(candidate.getDateOfBirth());
                candidateBasicsForm.setEmail(candidate.getContact().getEmail());
                candidateBasicsForm.setPhone(candidate.getContact().getPhone());
                candidateBasicsForm.setLinkedIn(candidate.getContact().getLinkedIn());
                candidateBasicsForm.setGitLab(candidate.getContact().getGitLab());
            }
            model.addAttribute("candidateBasicsForm", candidateBasicsForm);
            model.addAttribute("candidate", candidate);
            return "editCandidateBasics";
        } else if ("delete".equals(action)) {
            if (form.getCandidateId() != null && form.getCandidateId() != 0) {
                developerService.delete(form.getCandidateId());
                return chooseCandidate(model, status);
            }
            return chooseCandidate(model, status);
        } else return chooseCandidate(model, status);
    }

    @RequestMapping(value = "/editCandidate/saveBasics")
    public String saveBasics(
            @ModelAttribute("candidate") Candidate candidate,
            @Valid CandidateBasicsForm form,
            @ModelAttribute("educationListForm") EducationListForm educationForm,
            BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            return "editCandidateBasics";
        }

        candidate.setName(form.getName());
        candidate.setSurname(form.getSurname());
        candidate.setCareerGoal(form.getCareerGoal());
        candidate.setDateOfBirth(form.getDateOfBirth());
        candidate.setInterests(form.getInterests());
        candidate.getContact().setEmail(form.getEmail());
        candidate.getContact().setGitLab(form.getGitLab());
        candidate.getContact().setLinkedIn(form.getLinkedIn());
        candidate.getContact().setPhone(form.getPhone());

        educationForm.setEducationList(candidate.getEducationList());

        //  return editEducation(candidate, educationForm);
        return "redirect:/editCandidate/editEducation";
    }


    @RequestMapping(value = "/editCandidate/editEducation")
    public String editEducation(
            @ModelAttribute("candidate") Candidate candidate,
            @ModelAttribute("educationListForm") EducationListForm form) {


        if (form.getEducationList() == null) {
            List<Education> educationList = candidate.getEducationList();
            educationList.add(new Education());
            form.getEducationList().addAll(educationList);
        }
        return "editEducation";
    }

    @RequestMapping(value = "/editCandidate/editEducation", params = "add")
    public String addEducation(
            @ModelAttribute("educationListForm") EducationListForm form,
            BindingResult bindingResult, ModelMap model) {

        if (bindingResult.hasErrors()) {
            return "editEducation";
        }

        List<Education> educationList = form.getEducationList();
        Education newEdu = new Education();
        int nextViewId = 0;
        nextViewId = form.getViewIndex();
        newEdu.setViewId(String.valueOf(nextViewId));
        form.setViewIndex(nextViewId + 1);
        form.getEducationList().add(newEdu);

        return "editEducation";
    }

    @RequestMapping(value = "/editCandidate/editEducation", params = "remove")
    public String removeEducation(
            @RequestParam("remove") String remove,
            @ModelAttribute("educationListForm") EducationListForm form,
            BindingResult bindingResult, ModelMap model) {

        if (bindingResult.hasErrors()) {
            return "editEducation";
        }

// form.getEducationList().
        form.getEducationList().removeIf(n -> n.getViewId().equals(remove));

        return "editEducation";
    }

    @RequestMapping(value = "/editCandidate/editEducation", params = "saveAndProceed")
    public String saveEducation(
            @ModelAttribute("candidate") Candidate candidate,
            @ModelAttribute("educationListForm") EducationListForm form,
            @ModelAttribute("employmentListForm") EmploymentListForm empForm,
            BindingResult bindingResult, ModelMap model) {

        if (bindingResult.hasErrors()) {
            return "editEducation";
        }

        for (Education edu : candidate.getEducationList()) {
            candidate.bindToThis(edu);
        }
        developerService.persist((Developer) candidate);

        empForm.setEmploymentList(candidate.getWorkExperienceList());

        return "editEmployments";
    }

    @RequestMapping(value = "/editCandidate/editEmployments", params = "add")
    public String addEmployment(
            @ModelAttribute("employmentListForm") EmploymentListForm form,
            BindingResult bindingResult, ModelMap model) {

        if (bindingResult.hasErrors()) {
            return "editEmployments";
        }

        List<WorkExperience> employmentList = form.getEmploymentList();
        WorkExperience newEmp = new WorkExperience();
        int nextViewId = 0;
        nextViewId = form.getViewIndex();
        newEmp.setViewId(String.valueOf(nextViewId));
        form.setViewIndex(nextViewId + 1);
        form.getEmploymentList().add(newEmp);

        return "editEmployments";
    }

    @RequestMapping(value = "/editCandidate/editEmployments", params = "remove")
    public String removeEmployment(
            @ModelAttribute("employmentListForm") EmploymentListForm form,
            @RequestParam("remove") String remove,
            BindingResult bindingResult, ModelMap model) {

        if (bindingResult.hasErrors()) {
            return "editEmployments";
        }

        form.getEmploymentList().removeIf(n -> n.getViewId().equals(remove));

        return "editEmployments";
    }

    @RequestMapping(value = "/editCandidate/editEmployments", params = "saveAndProceed")
    public String saveEmployments(
            @ModelAttribute("candidate") Candidate candidate,
            @ModelAttribute("employmentListForm") EmploymentListForm form,
            BindingResult bindingResult, ModelMap model) {

        if (bindingResult.hasErrors()) {
            return "editEmployments";
        }

        for (WorkExperience employment : candidate.getWorkExperienceList()) {
            candidate.bindToThis(employment);
        }
        developerService.persist((Developer) candidate);

        return "chooseCandidate";
    }


}

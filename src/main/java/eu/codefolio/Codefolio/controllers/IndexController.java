package eu.codefolio.Codefolio.controllers;

import eu.codefolio.Codefolio.services.DeveloperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by verit on 21.10.2018.
 */
@Controller
public class IndexController {

    @Autowired
    private DeveloperService developerService;

    @RequestMapping (value={"/","/index.html"})
    public String getIndex (Model model) {
//        model.addAttribute("adam", developerService.getDeveloper(1L));
  //      model.addAttribute("tomek", developerService.getDeveloper(2L));
        return "index";
    }

}

package eu.codefolio.Codefolio.controllers;

import com.itextpdf.text.DocumentException;
import eu.codefolio.Codefolio.domains.Developer;
import eu.codefolio.Codefolio.services.CVPrintService;
import eu.codefolio.Codefolio.services.DeveloperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URISyntaxException;

/**
 * Created by verit on 21.10.2018.
 */
@Controller
public class DevController {
    @Autowired
    private DeveloperService developerService;

    @Autowired
    private CVPrintService cvPrintService;

    /*
    @RequestMapping ("/{name}")
    public String getDev (@PathVariable String name, Model model) {

        model.addAttribute("developer", developerService.getDeveloper(name));

        return "dev";
    }*/

    @RequestMapping(value="/{name}/printCV", produces="application/pdf", method= RequestMethod.GET)
    @ResponseBody
    public FileSystemResource printCV (@PathVariable String name, HttpServletResponse response, Model model) throws DocumentException, IOException, URISyntaxException {
        Developer developer = developerService.getDeveloper(name);
        model.addAttribute("developer", developer);

       cvPrintService.produceCV(developer);
       File file = new File(developer.getName() + developer.getSurname() + "_CV.pdf");
       file.setReadable(true);
       return new FileSystemResource(new File(developer.getName() + developer.getSurname() + "_CV.pdf"));
    }

}
package eu.codefolio.Codefolio.controllers;

import eu.codefolio.Codefolio.commands.LoginForm;
import eu.codefolio.Codefolio.commands.RegisterForm;
import eu.codefolio.Codefolio.domains.AutUser;
import eu.codefolio.Codefolio.domains.Role;
import eu.codefolio.Codefolio.services.SecurityService;
import eu.codefolio.Codefolio.services.UserService;
import org.hibernate.service.spi.InjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by tomasz.szczucki on 24.02.2019.
 */
@Controller
public class RegisterController {


    @Autowired
    UserService userService;

    @Autowired
    SecurityService securityService;


    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegForm(Model model) {
        model.addAttribute("registerForm", new RegisterForm());
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(Model model,RegisterForm regForm, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "register";
        }

        AutUser newAutUser = new AutUser();
        newAutUser.setEmail(regForm.getEmail());
        newAutUser.setPassword(regForm.getPassword());
        newAutUser.setRoles(new HashSet());
        userService.persist(newAutUser);

        securityService.autoLogin(newAutUser.getEmail(), newAutUser.getPasswordConfirm());


        return "redirect:/registerSuccessful";
    }


}


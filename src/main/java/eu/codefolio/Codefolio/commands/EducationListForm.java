package eu.codefolio.Codefolio.commands;

import eu.codefolio.Codefolio.domains.Education;

import java.util.List;

/**
 * Created by tomasz.szczucki on 26.01.2019.
 */
public class EducationListForm {

    public EducationListForm () {
    }

    public EducationListForm (List<Education> educationList) {
        this.educationList = educationList;
    }

    private List<Education> educationList;
    private int viewIndex = 0;


    public int getViewIndex() {
        return viewIndex;
    }

    public void setViewIndex(int viewIndex) {
        this.viewIndex = viewIndex;
    }

    public List<Education> getEducationList() {
        return educationList;
    }

    public void setEducationList(List<Education> educationList) {
        this.educationList = educationList;
    }

}

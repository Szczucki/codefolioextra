package eu.codefolio.Codefolio.commands;

/**
 * Created by tomasz.szczucki on 27.03.2019.
 */
public class RegisterForm {

    String email;
    String password;
    String passwordAgain;


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordAgain() {
        return passwordAgain;
    }

    public void setPasswordAgain(String passwordAgain) {
        this.passwordAgain = passwordAgain;
    }
}
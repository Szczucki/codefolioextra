package eu.codefolio.Codefolio.commands;

import eu.codefolio.Codefolio.domains.Education;
import eu.codefolio.Codefolio.domains.WorkExperience;

import java.util.List;
import java.util.Set;

/**
 * Created by tomasz.szczucki on 26.01.2019.
 */
public class EmploymentListForm {

    public EmploymentListForm() {
    }

    public EmploymentListForm(List<WorkExperience> employmentList) {
        this.employmentList = employmentList;
    }

    private List<WorkExperience> employmentList;

    public List<WorkExperience> getEmploymentList() {
        return employmentList;
    }

    public void setEmploymentList(List<WorkExperience> employmentList) {
        this.employmentList = employmentList;
    }

    private int viewIndex = 0;


    public int getViewIndex() {
        return viewIndex;
    }

    public void setViewIndex(int viewIndex) {
        this.viewIndex = viewIndex;
    }

}

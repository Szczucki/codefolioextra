package eu.codefolio.Codefolio.commands;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class ChooseCandidateForm {


    private Long candidateId;

    public Long getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(Long candidateId) {
        this.candidateId = candidateId;
    }
}

package eu.codefolio.Codefolio.persistence;

import eu.codefolio.Codefolio.domains.Developer;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by verit on 10.11.2018.
 */
public interface DeveloperRepository extends CrudRepository<Developer, Long> {
    Developer findByName(String name);
}

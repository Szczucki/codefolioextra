package eu.codefolio.Codefolio.persistence;

import eu.codefolio.Codefolio.domains.Role;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by tomasz.szczucki on 08.04.2019.
 */
public interface RoleRepository extends CrudRepository<Role, Long> {
}

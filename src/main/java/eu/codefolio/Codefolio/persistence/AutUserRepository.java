package eu.codefolio.Codefolio.persistence;

import eu.codefolio.Codefolio.domains.AutUser;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by tomasz.szczucki on 27.03.2019.
 */
public interface AutUserRepository extends CrudRepository<AutUser, Long> {
    AutUser findByEmail(String email);
}

package eu.codefolio.Codefolio.domains;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlIDREF;
import java.time.LocalDate;

/**
 * Created by verit on 07.12.2018.
 */
@Entity
public class WorkExperience {

    @Id
    @GeneratedValue
    private Long id;
    @Column (name = "COMPANY")
    private String company;
    @Column (name = "DATE_FROM")
    private LocalDate dateFrom;
    @Column (name = "DATE_TO")
    private LocalDate dateTo;
    @Column (name = "RESPONSIBILITIES")
    private String responsibilities;
    @Column (name = "POSITION")
    private String position;
    @ManyToOne
    @JoinColumn(name = "DEVELOPER_ID")
    private Developer developer;
    @Transient
    String viewId;

    public String getViewId() {
        return viewId;
    }

    public void setViewId(String viewId) {
        this.viewId = viewId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public String getResponsibilities() {
        return responsibilities;
    }

    public void setResponsibilities(String responsibilities) {
        this.responsibilities = responsibilities;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Developer getDeveloper() {
        return developer;
    }

    public void setDeveloper(Developer developer) {
        this.developer = developer;
    }
}

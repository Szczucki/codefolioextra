package eu.codefolio.Codefolio.domains;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by verit on 21.10.2018.
 */
@Entity(name = "DEVELOPER")
public class Developer implements Candidate {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "NAME")
    private String name;
    @Column(name = "SURNAME")
    private String surname;

    /*
    @ManyToMany(cascade=CascadeType.ALL)
    @JoinTable (name = "DEVELOPERS_PROJECTS", joinColumns = @JoinColumn(name = "DEVELOPER_ID"), inverseJoinColumns = @JoinColumn(name = "PROJECT_ID"))
    private List<Project> projects;
*/

    @OneToMany(mappedBy = "developer", cascade = CascadeType.ALL)
    private List<Project> projects;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "DEVELOPERS_SKILLS", joinColumns = @JoinColumn(name = "DEVELOPER_ID"), inverseJoinColumns = @JoinColumn(name = "SKILL_ID"))
    private List<Skill> skills;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "developer", cascade = CascadeType.ALL)
    private List<WorkExperience> workExperienceList;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "developer", cascade = CascadeType.ALL)
    private List<Education> educationList;

    @OneToOne(cascade = CascadeType.ALL)
    private Contact contact;

    @Column(name = "PHOTO")
    private String photo;

    @Column(name = "CAREER_GOAL")
    private String careerGoal;

    @Column(name = "INTERESTS")
    private String interests;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Column(name = "DATE_OF_BIRTH")
    private LocalDate dateOfBirth;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    AutUser AutUser;


    public Developer() {
        this.contact = new Contact();
        this.skills = new ArrayList<>();
        this.workExperienceList = new ArrayList<>();
        this.educationList = new ArrayList<>();
        this.projects = new ArrayList<>();
    }

    public void bindToThis(Education education) {
        if (education.getDeveloper() != this) {
            education.setDeveloper(this);
        }
    }

    public void bindToThis(WorkExperience workExperience) {
        if (workExperience.getDeveloper() != this) {
            workExperience.setDeveloper(this);
        }
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCareerGoal() {
        return careerGoal;
    }

    public void setCareerGoal(String careerGoal) {
        this.careerGoal = careerGoal;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<WorkExperience> getWorkExperienceList() {
        return workExperienceList;
    }

    public void setWorkExperienceList(List<WorkExperience> workExperienceList) {
        this.workExperienceList = workExperienceList;
    }

    public List<Education> getEducationList() {
        return educationList;
    }

    public void setEducationList(List<Education> educationList) {
        this.educationList = educationList;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public AutUser getAutUser() {
        return AutUser;
    }

    public void setAutUser(AutUser AutUser) {
        this.AutUser = AutUser;
    }
}



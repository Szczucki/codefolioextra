package eu.codefolio.Codefolio.domains;

import javax.persistence.*;
import java.util.List;

/**
 * Created by verit on 10.11.2018.
 */
@Entity(name = "SKILL")
public class Skill {

    @Id
    @GeneratedValue
    Long id;

    @Column(name = "NAME")
    private String name;
    @Column(name = "PROFICIENCY")
    private String proficiency;
    @Column(name = "PRIORITY")
    private Integer priority;

    @ManyToMany(mappedBy = "skills")
    private List<Developer> developers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProficiency() {
        return proficiency;
    }

    public void setProficiency(String proficiency) {
        this.proficiency = proficiency;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public List<Developer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<Developer> developers) {
        this.developers = developers;
    }
}

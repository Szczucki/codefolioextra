package eu.codefolio.Codefolio.domains;

import javax.persistence.*;
import java.util.List;

@Entity
public class Project {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name ="PROJECTNAME")
    private String projectName;
    @Column(name ="GITLINK")
    private String gitLink;
    @Column(name ="DESCRIPTION")
    private String description;
    @Column(name ="IMGLINK")
    private String imgLink;
    @Column(name ="DOWNLOADLINK")
    private String downloadLink;

    @ManyToOne
    @JoinColumn (name = "DEVELOPER_ID")
    private Developer developer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getGitLink() {
        return gitLink;
    }

    public void setGitLink(String gitLink) {
        this.gitLink = gitLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImgLink() {
        return imgLink;
    }

    public void setImgLink(String imgLink) {
        this.imgLink = imgLink;
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }


}

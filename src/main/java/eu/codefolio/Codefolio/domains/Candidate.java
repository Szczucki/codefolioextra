package eu.codefolio.Codefolio.domains;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by verit on 07.12.2018.
 */
public interface Candidate {

    Long getId();
    String getName();
    String getSurname();
    String getCareerGoal();
    Contact getContact();
    String getInterests();
    List<Education> getEducationList();
    List<WorkExperience> getWorkExperienceList();
    String getPhoto();
    List<Skill> getSkills();

    void setName(String name);

    LocalDate getDateOfBirth();

    void setContact(Contact contact);

    void setSurname(String surname);

    void setCareerGoal(String careerGoal);

    void setDateOfBirth(LocalDate dateOfBirth);

    void setInterests(String interests);

    void bindToThis(Education education);

    void bindToThis(WorkExperience employment);

    void setId(Long id);
}

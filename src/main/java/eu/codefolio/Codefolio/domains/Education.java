package eu.codefolio.Codefolio.domains;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Created by verit on 07.12.2018.
 */
@Entity
public class Education {

    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "SCHOOL")
    private String school;
    @Column (name = "DATE_FROM")
    private LocalDate dateFrom;
    @Column (name = "DATE_TO")
    private LocalDate dateTo;
    @Column (name = "FIELD_OF_STUDY")
    private String fieldOfStudy;
    @Column (name = "DEGREE")
    private String degree;
    @ManyToOne
    @JoinColumn (name = "DEVELOPER_ID")
    private Developer developer;
    @Transient
    private String viewId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public String getFieldOfStudy() {
        return fieldOfStudy;
    }

    public void setFieldOfStudy(String fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public Developer getDeveloper() {
        return developer;
    }

    public void setDeveloper(Developer developer) {
        this.developer = developer;
    }

    public String getViewId() {
        return viewId;
    }

    public void setViewId(String viewId) {
        this.viewId = viewId;
    }
}

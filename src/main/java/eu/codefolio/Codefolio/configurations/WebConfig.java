package eu.codefolio.Codefolio.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created by tomasz.szczucki on 04.03.2019.
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
}

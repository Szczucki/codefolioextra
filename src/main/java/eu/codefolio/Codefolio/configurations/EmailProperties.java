package eu.codefolio.Codefolio.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by tomasz.szczucki on 04.12.2018.
 */
@Configuration
@ConfigurationProperties (prefix = "spring.mail")
public class EmailProperties {

          //  spring.mail.properties.mail.smtp.auth=true
  //  spring.mail.properties.mail.smtp.starttls.enable=true

    private String host;
    private Integer port;
    private String username;
    private String password;
    private String smptAuth;
    private String smtpStarttlsEnable;
    private String socketFactoryClass;
    private String debug;
    private String protocol;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSmptAuth() {
        return smptAuth;
    }

    public void setSmptAuth(String smptAuth) {
        this.smptAuth = smptAuth;
    }

    public String getSmtpStarttlsEnable() {
        return smtpStarttlsEnable;
    }

    public void setSmtpStarttlsEnable(String smtpStarttlsEnable) {
        this.smtpStarttlsEnable = smtpStarttlsEnable;
    }

    public String getSocketFactoryClass() {
        return socketFactoryClass;
    }

    public void setSocketFactoryClass(String socketFactoryClass) {
        this.socketFactoryClass = socketFactoryClass;
    }

    public String getDebug() {
        return debug;
    }

    public void setDebug(String debug) {
        this.debug = debug;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }
}
